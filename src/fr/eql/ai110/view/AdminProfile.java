package fr.eql.ai110.view;

import java.io.FileNotFoundException;

import fr.eql.ai110.model.Research;
import fr.eql.ai110.model.TraineeDAO;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AdminProfile extends HBox {
	private Button profileButton = new Button();
	private Button loginOutButton = new Button("Se déconnecter");
	private Button logoButton = new Button();
	private Image profileIcon = new Image(getClass().getResourceAsStream("./img/profile_icon.png"));
	private Image logo = new Image(getClass().getResourceAsStream("./img/logo.png"));
	private HBox hbox1= new HBox();
	private HBox hbox2= new HBox();
	private StackPane stackPane = new StackPane();


	public AdminProfile() {

		hbox1.getChildren().add(profileButton);
		hbox1.setAlignment(Pos.TOP_RIGHT);
		hbox1.setPrefWidth(1900);
		hbox2.getChildren().add(logoButton);
		hbox2.setAlignment(Pos.TOP_LEFT);

		logoButton.setGraphic(new ImageView(logo));
		logoButton.setStyle("-fx-background-color:transparent");
		stackPane.getChildren().addAll(hbox2, hbox1);

		profileButton.setGraphic(new ImageView(profileIcon));
		profileButton.setStyle(
				"-fx-background-radius: 80px; " +
						"-fx-min-width: 60px; " +
						"-fx-min-height: 60px; " +
						"-fx-max-width: 60px; " +
						"-fx-max-height: 60px;"
				);

		loginOutButton.setStyle("-fx-font-size: 20px ; -fx-background-color:transparent");
		loginOutButton.setPadding(new Insets(15,20,0,0));


		setPadding(new Insets (20, 20, 10, 20));
		getChildren().addAll(stackPane);
		setPrefHeight(80);
		setStyle ("-fx-background-color:#A398CD");


		hbox1.getChildren().add(0, loginOutButton);
		loginOutButton.setVisible(false);


		profileButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				loginOutButton.setVisible(true);

			}
		});


		profileButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent e) {
				loginOutButton.setVisible(false);
			}
		});

		



	}


	public Button getLoginInButton() {
		return loginOutButton;
	}

	public Button getProfileButton() {
		return profileButton;
	}



}
