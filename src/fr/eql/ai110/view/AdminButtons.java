package fr.eql.ai110.view;

import java.io.IOException;

import fr.eql.ai110.model.TraineeDAO;
import fr.eql.ai110.model.WriteTraineeListInPDF;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

public class AdminButtons extends VBox {

	private Image editIcon = new Image(getClass().getResourceAsStream("./img/edit_icon.png"));
	private Image deleteIcon = new Image(getClass().getResourceAsStream("./img/delete_icon.png"));
	private Image exportIcon = new Image(getClass().getResourceAsStream("./img/export_icon.png"));
	private Button editButton = new Button();
	private Button deleteButton = new Button();
	private Button exportButton = new Button();
	
	public AdminButtons() {
		
	
		editButton.setGraphic(new ImageView(editIcon));
		deleteButton.setGraphic(new ImageView(deleteIcon));
		exportButton.setGraphic(new ImageView(exportIcon));
		
		editButton.setTooltip(new Tooltip("Mettre à jour la ligne sélectionnée"));
		deleteButton.setTooltip(new Tooltip("Supprimer la ligne sélectionnée"));
		exportButton.setTooltip(new Tooltip("Exporter la liste en PDF"));
		
		
		editButton.setPadding(new Insets (0, 0, 0, 5));
		deleteButton.setPadding(new Insets (0, 0, 0, 5));
		exportButton.setPadding(new Insets (0, 0, 0, 7));
		
		editButton.setAlignment(Pos.CENTER_LEFT);
		deleteButton.setAlignment(Pos.CENTER_LEFT);
		exportButton.setAlignment(Pos.CENTER_LEFT);
		
		editButton.setStyle(
	            "-fx-background-radius: 50px; " +
	            "-fx-min-width: 50px; " +
	            "-fx-min-height: 50px; " +
	            "-fx-max-width: 50px; " +
	            "-fx-max-height: 50px;" 
	    );
		
		deleteButton.setStyle(
	            "-fx-background-radius: 50px; " +
	            "-fx-min-width: 50px; " +
	            "-fx-min-height: 50px; " +
	            "-fx-max-width: 50px; " +
	            "-fx-max-height: 50px;"
	    );
		
		exportButton.setStyle(
	            "-fx-background-radius: 50px; " +
	            "-fx-min-width: 50px; " +
	            "-fx-min-height: 50px; " +
	            "-fx-max-width: 50px; " +
	            "-fx-max-height: 50px;"
	    );
		
		/*Méthode export sur bouton côté user*/
		
//		exportButton.setOnAction( (event) -> {
//			
//			WriteTraineeListInPDF wt = new WriteTraineeListInPDF();
//			TraineeDAO tdao = new TraineeDAO();
//			
//			UserPanel up = (UserPanel) AdminButtons.this.getScene().getRoot();
//
//			try {
//				wt.writePDF(up.getTableViewTrainee().getObservableTrainees());
//				
//			} catch (IOException e1) {
//				e1.printStackTrace();
//			}
//		});
		
		/**********************************************************/
		
		editButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	  editButton.setEffect(new DropShadow());
	        	 
	          }
	        });

       
       editButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	 editButton.setEffect(null);
	          }
	        });
		
       
		deleteButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	  deleteButton.setEffect(new DropShadow());
	        	 
	          }
	        });

     
		deleteButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	 deleteButton.setEffect(null);
	          }
	        });
       
		exportButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	  exportButton.setEffect(new DropShadow());
	        	 
	          }
	        });

     
		exportButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	 exportButton.setEffect(null);
	          }
	        });
		
		
		
		
        getChildren().addAll(editButton, deleteButton, exportButton);
//        setStyle ("-fx-background-color:lightblue");
        setAlignment(Pos.BOTTOM_LEFT);
        setPadding(new Insets (0, 0, 50, 0));
        setPrefWidth(70);
        setSpacing(20);
        
        
        
	}

	public Button getEditButton() {
		return editButton;
	}

	public void setEditButton(Button editButton) {
		this.editButton = editButton;
	}

	public Button getDeleteButton() {
		return deleteButton;
	}

	public void setDeleteButton(Button deleteButton) {
		this.deleteButton = deleteButton;
	}

	public Button getExportButton() {
		return exportButton;
	}

	public void setExportButton(Button exportButton) {
		this.exportButton = exportButton;
	}
	
	
	
}
