package fr.eql.ai110.view;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import fr.eql.ai110.model.Research;
import fr.eql.ai110.model.SearchTrainee;
import fr.eql.ai110.model.Trainee;
import fr.eql.ai110.model.TraineeDAO;
import fr.eql.ai110.model.WriteTraineeListInPDF;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class FormSearchPanel extends VBox {


	private Label lblSearch = new Label ("Recherche");
	private Label lblFirstName = new Label ("Nom");
	private TextField txtFirstName = new TextField();
	private Label lblName = new Label ("Prénom");
	private TextField txtName  = new TextField();
	private Label lblRegion = new Label ("Département");
	private TextField txtRegion  = new TextField();
	private Label lblPromotion = new Label ("Promotion");
	private TextField txtPromotion  = new TextField();
	private Label lblYear = new Label ("Année");
	private TextField txtYear  = new TextField();

	private Button searchButton = new Button("Rechercher");
	private Button exportButton = new Button();

	
	
	public FormSearchPanel() {
		

		//Mise en forme des labels
		lblSearch.setPrefSize(300, 30);
		lblSearch.setPadding(new Insets (10, 0, 0, 60));
		lblSearch.setStyle("-fx-font-size:30px");

//		lblSearch.setAlignment(Pos.TOP_LEFT);
		
		lblFirstName.setStyle("-fx-font-weight: bold");

		lblSearch.setTextFill(Color.web("#76448A"));

		lblFirstName.setStyle("-fx-font-weight: bold");

		lblName.setStyle("-fx-font-weight: bold");
		lblRegion.setStyle("-fx-font-weight: bold");
		lblPromotion.setStyle("-fx-font-weight: bold");
		lblYear.setStyle("-fx-font-weight: bold");

		
		//Création du couple label+textfield dans une VBox
		VBox vbox1 = new VBox();
		vbox1.getChildren().addAll(lblFirstName,txtFirstName);
		VBox vbox2 = new VBox();
		vbox2.getChildren().addAll(lblName,txtName);
		VBox vbox3 = new VBox();
		vbox3.getChildren().addAll(lblRegion,txtRegion);
		VBox vbox4 = new VBox();
		vbox4.getChildren().addAll(lblPromotion,txtPromotion);
		VBox vbox5 = new VBox();
		vbox5.getChildren().addAll(lblYear,txtYear);
		
		//Ajout des vbox dans une HBox
		HBox hbox1 = new HBox();
		hbox1.getChildren().addAll(vbox1, vbox2, vbox3, vbox4, vbox5);
		hbox1.setSpacing(80);
		hbox1.setStyle("-fx-font-size: 16px");
		hbox1.setPadding(new Insets (0, 0, 10, 100));
		
		
//---------modif pour intégrer Research commence ici------------
		searchButton.setOnAction( (event) -> {
			TraineeDAO td = new TraineeDAO();
			Research rs = new Research();			
			try {
			
				UserPanel up = (UserPanel) FormSearchPanel.this.getScene().getRoot();
				
				up.getTableViewTrainee().getObservableTrainees().clear();
				if(txtFirstName.getText().length()==0) {
					up.getTableViewTrainee().getObservableTrainees().addAll(rs.reseachByYear(rs.reseachByPromotion(rs.reseachByRegion(rs.reseachByName
					(rs.reseachByFirstName(td.getAll(),txtFirstName.getText().toUpperCase().replace(" ", "")), 
							txtName.getText().toUpperCase().replace(" ", "")), txtRegion.getText().toUpperCase().replace(" ", "")), txtPromotion.getText().toUpperCase().replace(" ", "")), txtYear.getText()));
				}else {
				up.getTableViewTrainee().getObservableTrainees().addAll(rs.reseachByYear(rs.reseachByPromotion(rs.reseachByRegion(rs.reseachByName
						(rs.reseachByFirstName(td.getAll(), txtFirstName.getText().toUpperCase().replace(" ", "")), 
								txtName.getText().toUpperCase().replace(" ", "")), txtRegion.getText().toUpperCase().replace(" ", "")), txtPromotion.getText().toUpperCase().replace(" ", "")), txtYear.getText()));
				}
				
			} catch (FileNotFoundException e) {	
				e.printStackTrace();
			}
			
		});
//---------modif pour integrer Research termine ici--------------
		

		
		searchButton.setPrefSize(240, 40);
		searchButton.setPadding(new Insets (10, 0, 10, 0));
		searchButton.setStyle("-fx-font-weight: bold;" + 
				"-fx-font-size: 18px; " +
	            "-fx-background-radius: 80px; " 

	    );
		VBox searchButtonPanel = new VBox();
		searchButtonPanel.getChildren().add(searchButton);
		searchButtonPanel.setAlignment(Pos.CENTER);
		
		getChildren().addAll(lblSearch, hbox1, searchButtonPanel);
		setSpacing(15);

		

	}



	public Label getLblSearch() {
		return lblSearch;
	}



	public void setLblSearch(Label lblSearch) {
		this.lblSearch = lblSearch;
	}



	public Label getLblFirstName() {
		return lblFirstName;
	}



	public void setLblFirstName(Label lblFirstName) {
		this.lblFirstName = lblFirstName;
	}



	public TextField getTxtFirstName() {
		return txtFirstName;
	}



	public void setTxtFirstName(TextField txtFirstName) {
		this.txtFirstName = txtFirstName;
	}



	public Label getLblName() {
		return lblName;
	}



	public void setLblName(Label lblName) {
		this.lblName = lblName;
	}



	public TextField getTxtName() {
		return txtName;
	}



	public void setTxtName(TextField txtName) {
		this.txtName = txtName;
	}



	public Label getLblRegion() {
		return lblRegion;
	}



	public void setLblRegion(Label lblRegion) {
		this.lblRegion = lblRegion;
	}



	public TextField getTxtRegion() {
		return txtRegion;
	}



	public void setTxtRegion(TextField txtRegion) {
		this.txtRegion = txtRegion;
	}



	public Label getLblPromotion() {
		return lblPromotion;
	}



	public void setLblPromotion(Label lblPromotion) {
		this.lblPromotion = lblPromotion;
	}



	public TextField getTxtPromotion() {
		return txtPromotion;
	}



	public void setTxtPromotion(TextField txtPromotion) {
		this.txtPromotion = txtPromotion;
	}



	public Label getLblYear() {
		return lblYear;
	}



	public void setLblYear(Label lblYear) {
		this.lblYear = lblYear;
	}



	public TextField getTxtYear() {
		return txtYear;
	}



	public void setTxtYear(TextField txtYear) {
		this.txtYear = txtYear;
	}



	public Button getSearchButton() {
		return searchButton;
	}



	public void setSearchButton(Button searchButton) {
		this.searchButton = searchButton;
	}

	
	
}
