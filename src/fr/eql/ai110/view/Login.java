package fr.eql.ai110.view;

import java.io.FileNotFoundException;

import fr.eql.ai110.model.Admin;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Login extends VBox {

	private Label lblLogin = new Label("Connexion Administrateur");
	private GridPane grid = new GridPane();
	private Label lblUsername = new Label("Nom d'utilisateur");
	private TextField txtUsername = new TextField();
	private Label lblPassword = new Label("Mot de passe");
	private PasswordField txtPassword = new PasswordField();
	private Button logInBtn = new Button("Se connecter");
	private Label messageError = new Label();
	private VBox vbox = new VBox();
	
	public Login() {
		
		grid.addRow(0, lblUsername, lblPassword);
		grid.addRow(1, txtUsername, txtPassword);
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(80);
		grid.setVgap(10);
		grid.setPadding(new Insets (10, 0, 40, 0));
		
		lblLogin.setStyle("-fx-font-size:30px ; -fx-text-fill:white ; -fx-font-weight: bold");
//		lblLogin.setTextFill(Color.web("#A7B9BC"));
		lblLogin.setPadding(new Insets (0, 0, 50, 0));
		lblUsername.setStyle("-fx-font-weight: bold ; -fx-font-size:20px ; -fx-text-fill:white");
		lblPassword.setStyle("-fx-font-weight: bold ; -fx-font-size:20px ; -fx-text-fill:white");
		txtUsername.setStyle("-fx-font-size:18px");
		txtPassword.setStyle("-fx-font-size:18px");
		
		logInBtn.setStyle("-fx-font-size:18px ");
		logInBtn.setPadding(new Insets (10, 20, 10, 20));

		
		messageError.setStyle ("-fx-text-fill:red ; -fx-font-size:22px ; -fx-background-color:white");
		vbox.getChildren().addAll(lblLogin, grid, logInBtn, messageError);
		vbox.setStyle ("-fx-background-color:#8E989A");
		vbox.setAlignment(Pos.CENTER);
		vbox.setSpacing(30);
		vbox.setMaxSize(800, 500);
		vbox.setMinSize(800, 500);
		getChildren().addAll(vbox);
		
		setSpacing(30);
		setAlignment(Pos.CENTER);
		
		
		/*
		 * Se connecter à l'interface admin	
		 */
		logInBtn.setOnAction( event -> {
			try {			
			String userName;
			String passWord;
			
			Admin adm = new Admin();
			if (adm.checkAccount(txtUsername.getText()) == true && adm.account().equals(txtPassword.getText()) ) {
				Parent scene = new AdminPanel();
				this.getScene().setRoot(scene);
			} else {
//				
				messageError.setText("Identifiant ou mot de passe incorrect");
				
			}
			
			
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		
	}
	
	
	
}
