package fr.eql.ai110.view;

import java.io.FileNotFoundException;
import java.io.InputStream;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

public class Menu extends VBox {

	private Image importIcon = new Image(getClass().getResourceAsStream("./img/import_icon.png"));
	private Image searchIcon = new Image(getClass().getResourceAsStream("./img/search_icon.png"));
	private Image addIcon = new Image(getClass().getResourceAsStream("./img/add_icon.png"));
	private Image listIcon = new Image(getClass().getResourceAsStream("./img/list_icon.png"));
	private Image guideIcon = new Image(getClass().getResourceAsStream("./img/guide.png"));
	private Button importButton = new Button(" Import");
	private Button searchButton = new Button("Recherche");
	private Button addButton = new Button("  Ajout");
	private Button listButton = new Button("  Liste");
	private Button guideButton = new Button();
	private Label lblGuide = new Label("Télécharger\nle guide utilisateur");
	private VBox guideBox = new VBox();
	
	public Menu() {

		importButton.setGraphic(new ImageView(importIcon));
		listButton.setGraphic(new ImageView(listIcon));
		searchButton.setGraphic(new ImageView(searchIcon));
		addButton.setGraphic(new ImageView(addIcon));
		guideButton.setGraphic(new ImageView(guideIcon));
	
		importButton.setPrefSize(170., 60.);
		listButton.setPrefSize(170., 60.);
		searchButton.setPrefSize(170., 60.);
		addButton.setPrefSize(170., 60.);
		
		importButton.setAlignment(Pos.CENTER_LEFT);
		listButton.setAlignment(Pos.CENTER_LEFT);
		searchButton.setAlignment(Pos.CENTER_LEFT);
		addButton.setAlignment(Pos.CENTER_LEFT);
		guideBox.setAlignment(Pos.CENTER);
		lblGuide.setTextAlignment(TextAlignment.CENTER);
		
		
		importButton.setStyle("-fx-font-size: 18px");
		listButton.setStyle("-fx-font-size: 18px");
		searchButton.setStyle("-fx-font-size: 18px");
		addButton.setStyle("-fx-font-size: 18px");
		guideButton.setStyle(
	            "-fx-background-radius: 80px; " +
	            "-fx-min-width: 70px; " +
	            "-fx-min-height: 70px; " +
	            "-fx-max-width: 70px; " +
	            "-fx-max-height: 70px;" 
	    );
		
		guideButton.setPadding(new Insets(0,0,0,10));
		lblGuide.setPadding(new Insets(100,0,20,0));
		lblGuide.setVisible(false);
		lblGuide.setStyle("-fx-font-weight: bold");
		
		
		guideBox.getChildren().addAll(lblGuide, guideButton);
        getChildren().addAll(importButton, listButton, searchButton, addButton, guideBox);
//      setStyle ("-fx-background-color:lightblue");
    	setPadding(new Insets (200, 0, 0, 20));
        setAlignment(Pos.CENTER);
        setSpacing(50);
        
        
        guideButton.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	  lblGuide.setVisible(true);
	        	  guideButton.setEffect(new DropShadow());
	        	 
	          }
	        });

        
        guideButton.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
	          @Override
	          public void handle(MouseEvent e) {
	        	  lblGuide.setVisible(false);
	        	  guideButton.setEffect(null);
	          }
	        });
        
        
        
      
        
	}

	public Button getSearchButton() {
		return searchButton;
	}

	public void setSearchButton(Button searchButton) {
		this.searchButton = searchButton;
	}

	public Button getAddButton() {
		return addButton;
	}

	public void setAddButton(Button addButton) {
		this.addButton = addButton;
	}

	
	public Button getListButton() {
		return listButton;
	}

	public void setListButton(Button listButton) {
		this.listButton = listButton;
	}

	public Image getGuideIcon() {
		return guideIcon;
	}

	public void setGuideIcon(Image guideIcon) {
		this.guideIcon = guideIcon;
	}

	public Button getGuideButton() {
		return guideButton;
	}

	public void setGuideButton(Button guideButton) {
		this.guideButton = guideButton;
	}

	public Button getImportButton() {
		return importButton;
	}

	public void setImportButton(Button importButton) {
		this.importButton = importButton;
	}

	
	
}
