package fr.eql.ai110.view;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import fr.eql.ai110.model.Trainee;
import fr.eql.ai110.model.TraineeDAO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;




public class TableViewTrainee extends AnchorPane {
	
	ObservableList<Trainee> observableTrainees = FXCollections.observableArrayList();
	private List <Trainee> trainees = new ArrayList<>();
	private TableView<Trainee> tableView = null;
	
	
	public TableViewTrainee() throws FileNotFoundException {


	TraineeDAO dao = new TraineeDAO();
	observableTrainees = FXCollections.observableArrayList(dao.getAll());
	tableView = new TableView<Trainee>(observableTrainees);


	//Paramétrage du tableView
	TableColumn<Trainee, String> colFirstName = new TableColumn<Trainee, String>("Nom");
	colFirstName.setCellValueFactory(new PropertyValueFactory<Trainee, String>("firstName"));

	TableColumn<Trainee, String> colName = new TableColumn<Trainee, String>("Prénom");
	colName.setCellValueFactory(new PropertyValueFactory<Trainee, String>("name"));

	TableColumn<Trainee, String> colRegion = new TableColumn<Trainee, String>("Département");
	colRegion.setCellValueFactory(new PropertyValueFactory<Trainee, String>("region"));
	
	TableColumn<Trainee, String> colPromotion = new TableColumn<Trainee, String>("Promotion");
	colPromotion.setCellValueFactory(new PropertyValueFactory<Trainee, String>("promotion"));

	TableColumn<Trainee, String> colYear = new TableColumn<Trainee, String>("Année");
	colYear.setCellValueFactory(new PropertyValueFactory<Trainee, String>("year"));
	
	//On donne les colonnes au tableView
	tableView.getColumns().addAll(colFirstName, colName, colRegion, colPromotion, colYear);

	//ajuste la taille du tableau à son contenu
	getChildren().add(tableView);
	setPrefSize(1000, 1000);

	tableView.setColumnResizePolicy(tableView.CONSTRAINED_RESIZE_POLICY);

	//J'ancre la tableView sur les 4 cotés de mon anchorPane
	setTopAnchor(tableView, 20.);
	setBottomAnchor(tableView, 20.);
	setRightAnchor(tableView, 20.);
	setLeftAnchor(tableView, 20.);


	
	}

	public ObservableList<Trainee> getObservableTrainees() {
		return observableTrainees;
	}

	public void setObservableTrainees(ObservableList<Trainee> observableTrainees) {
		this.observableTrainees = observableTrainees;
	}

	public TableView<Trainee> getTableView() {
		return tableView;
	}

	public void setTableView(TableView<Trainee> tableView) {
		this.tableView = tableView;
	}

	
//	

	

}
