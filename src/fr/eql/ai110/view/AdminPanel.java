package fr.eql.ai110.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.soap.Node;

import fr.eql.ai110.model.RafManager;
import fr.eql.ai110.model.Research;
import fr.eql.ai110.model.Trainee;
import fr.eql.ai110.model.TraineeDAO;
import fr.eql.ai110.model.WriteTraineeListInPDF;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class AdminPanel extends BorderPane {
	private FormSearchPanel form = new FormSearchPanel();
	private Menu menu = new Menu();
	private AdminProfile profile = new AdminProfile();
	private TableViewTrainee tableViewPanel = new TableViewTrainee();
	private VBox centerPanelSearch = new VBox(form, tableViewPanel);
	private AddForm addPanel = new AddForm();
	private AdminButtons adminButtons = new AdminButtons();
	private ConsultPanel listPanel = new ConsultPanel();
	private Button editBtn = adminButtons.getEditButton();
	private Button deleteBtn = adminButtons.getDeleteButton();
	private Button exportButton = adminButtons.getExportButton();
	private ObservableList<Trainee> observableTrainees = FXCollections.observableArrayList();
	private TraineeDAO dao = new TraineeDAO();
	private Label lblList = listPanel.getLblConsult();
	private Research rs = new Research();
	private static LocalDate date = LocalDate.now();
	
	
	
	public AdminPanel() throws FileNotFoundException {
		
		
		setTop(profile);
		setLeft(menu);
		setCenter(listPanel);
		setRight(adminButtons);
		editBtn.setVisible(false);
		deleteBtn.setVisible(false);

	
		setStyle ("-fx-background-color:#D5CCF5");
		
		/*************************************************
		 *                   Left menu                   *
		 ************************************************/
		Button searchBtn = menu.getSearchButton();
		searchBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tableViewPanel.getObservableTrainees().clear();
				tableViewPanel.getObservableTrainees().addAll(dao.getAll());
				centerPanelSearch.getChildren().clear();
				centerPanelSearch.getChildren().addAll(form, tableViewPanel);
				setCenter(centerPanelSearch);
				setRight(adminButtons);
				editBtn.setVisible(true);
				deleteBtn.setVisible(true);
			}
		});
		
		
		Button addBtn = menu.getAddButton();
		addBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				setCenter(addPanel);
				setRight(null);
				tableViewPanel.getObservableTrainees().clear();
				tableViewPanel.getObservableTrainees().addAll(dao.getAll());
				
			}
			
		});
	
		
		Button listBtn = menu.getListButton();
		listBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tableViewPanel.getObservableTrainees().clear();
				tableViewPanel.getObservableTrainees().addAll(dao.getAll());
				listPanel.getChildren().clear();
				listPanel.getChildren().addAll(lblList, tableViewPanel);
				setCenter(listPanel);
				setRight(adminButtons);
				editBtn.setVisible(false);
				deleteBtn.setVisible(false);
			}
			
		});
		
		/*
		 * Action on user guide button
		 */
		Button guideBtn = menu.getGuideButton();
		guideBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				DirectoryChooser dirChooser = new DirectoryChooser ();
				dirChooser.setTitle("Sélectionner un dossier");
				File initDir =  new File(System.getProperty("user.home") + "/Desktop");
				dirChooser.setInitialDirectory(initDir);
				Parent root;
				try {
					root = new AdminPanel();
					Scene scene = new Scene(root);
					File dir = dirChooser.showDialog(scene.getWindow());
					String guidePath = dir.getAbsolutePath();  
				    File target = new File(guidePath+"\\Guide_utilisateur.pdf");
					File source = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\Guide_utilisateur.pdf");
				    copy(source, target);
				
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
	
			}
			
		});
		
		
			
		/*
		 * Log off admin interface 	
		 */
		Button logOutButton = profile.getProfileButton();	
		logOutButton.setOnAction( event -> {
			try {			
				Parent scene = new UserPanel();
				this.getScene().setRoot(scene);

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		});
		
		

		
		//Action on searchButton
		Button searchButton = form.getSearchButton();
		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					tableViewPanel.getObservableTrainees().clear();
					tableViewPanel.getObservableTrainees().addAll(dao.getAll());
					centerPanelSearch.getChildren().clear();
					centerPanelSearch.getChildren().addAll(form, tableViewPanel);
					searchMulticriteria();
					setCenter(centerPanelSearch);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}

		});
		
		
		
		/*Méthode export sur bouton côté admin*/
		exportButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				DirectoryChooser dirChooser = new DirectoryChooser ();
				dirChooser.setTitle("Sélectionner un dossier");
				File initDir =  new File(System.getProperty("user.home") + "/Desktop");
				dirChooser.setInitialDirectory(initDir);
				
				WriteTraineeListInPDF wt = new WriteTraineeListInPDF();
				
				try {
					wt.writePDF(tableViewPanel.getObservableTrainees());
					
					Parent root = new AdminPanel();
					Scene scene = new Scene(root);
					File dir = dirChooser.showDialog(scene.getWindow());
					String guidePath = dir.getAbsolutePath();  
				    File target = new File(guidePath+"\\"+date+"-stagiaires.pdf");
					File source = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\stagiaires.pdf");
				    copy(source, target);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
	
		
		/*Méthode import sur bouton côté admin*/
		Button importButton = menu.getImportButton();
		importButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Sélectionner le fichier source");
				File initDir = new File(System.getProperty("user.home") + "/Desktop");
				fileChooser.setInitialDirectory(initDir);
				File f = fileChooser.showOpenDialog(null);
				String sourceFilePath = f.getAbsolutePath();
				System.out.println(sourceFilePath);
				
				try {
					File fileToDelete = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\stagiaires.RAF");
					boolean isDeleted = fileToDelete.delete();
					System.out.println("Est supprimé : "+ isDeleted);
					File source = new File(sourceFilePath);
					File target = new File(System.getProperty("user.dir")+"\\src\\fr\\eql\\ai110\\data\\stagiaires.txt");
				    copy(source, target);
				    
				    RafManager rm = new RafManager();
					rm.readSourceFile();
					rm.formatRafData();			
					tableViewPanel.getObservableTrainees().clear();
					tableViewPanel.getObservableTrainees().addAll(dao.getAll());
					listPanel.getChildren().clear();
					listPanel.getChildren().addAll(lblList, tableViewPanel);
					setCenter(listPanel);
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		});

		
		
		/**********************************************************************************************************************************/
		/*
		 * Actions sur une ligne sélectionnée
		 */
		tableViewPanel.getTableView().getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Trainee>() {

			@Override
			public void changed(ObservableValue<? extends Trainee> observable, Trainee oldValue, Trainee newValue) {
				
				/*
				 * Action to edit trainee
				 */
				editBtn.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						showDialogToEdit(newValue);
						tableViewPanel.getObservableTrainees().clear();
						tableViewPanel.getObservableTrainees().addAll(dao.getAll());
						setCenter(centerPanelSearch);
					}
				});
				
				/*
				 * Action to delete trainee
				 */
				deleteBtn.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						showAlertToDelete(newValue);
						tableViewPanel.getObservableTrainees().clear();
						tableViewPanel.getObservableTrainees().addAll(dao.getAll());
						setCenter(centerPanelSearch);
					}
				});
				
				
				/**********************************************************************************************************************************************/
			}
		});

		
		//Action on addButton
		Button addButton = addPanel.getAddButton();
		Label addLabel = addPanel.getLblAdd();
		Label resultLabel = addPanel.getLblResult();
		GridPane addGrid = addPanel.getForm();
		addButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String txtFirstName = addPanel.getTxtFirstname().getText().toUpperCase().trim();
				String txtName = addPanel.getTxtName().getText().trim();
				String txtRegion = addPanel.getTxtRegion().getText().trim();
				String txtPromotion = addPanel.getTxtPromotion().getText().trim();
				String txtYear = addPanel.getTxtYear().getText().trim();
				
				Trainee newTrainee = new Trainee(txtFirstName, txtName, txtRegion, txtPromotion, txtYear);
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("MESSAGE");
				boolean isAdded = dao.addTrainee(newTrainee);
				if(isAdded) {
					alert.setHeaderText("Ajout du stagiaire réussi");
					ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
					alert.getButtonTypes().setAll(okButton);
					alert.showAndWait();
					
				}else {
					alert.setHeaderText("Echec de l'ajout");
					ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
					alert.getButtonTypes().setAll(okButton);
					alert.showAndWait();
				}
				tableViewPanel.getObservableTrainees().clear();
				tableViewPanel.getObservableTrainees().addAll(dao.getAll());
				addPanel.getTxtFirstname().setText("");
				addPanel.getTxtName().setText("");
				addPanel.getTxtRegion().setText("");
				addPanel.getTxtPromotion().setText("");
				addPanel.getTxtYear().setText("");
				setCenter(addPanel);

			
			}

		});

		
		
		
		
		
		
		
		
	}
	
	//**********************************************************
	//*                      CLASS METHOD                      *
	//**********************************************************
	
	
	//Search trainee By 
	private ObservableList<Trainee> searchByFirstName() throws FileNotFoundException {
		String txtFirstname = form.getTxtFirstName().getText();
		observableTrainees = FXCollections.observableArrayList(dao.searchByFirstname(txtFirstname));
		return observableTrainees;
	}
	
	
	
	/*
	 * Multicriteria search
	 */
	private void searchMulticriteria() throws FileNotFoundException {
		observableTrainees = FXCollections.observableArrayList(dao.getAll());
		String txtFirstName = form.getTxtFirstName().getText().toUpperCase().replace(" ", "");
		String txtName = form.getTxtName().getText().toUpperCase().replace(" ", "");
		String txtRegion = form.getTxtRegion().getText().toUpperCase().replace(" ", "");
		String txtPromotion = form.getTxtPromotion().getText().toUpperCase().toUpperCase().replace(" ", "");
		String txtYear = form.getTxtYear().getText().replace(" ", "");
						
		tableViewPanel.getObservableTrainees().clear();
	
		if(txtFirstName.length()!=0) {
			tableViewPanel.getObservableTrainees().addAll(rs.reseachByYear(rs.reseachByPromotion(rs.reseachByRegion(rs.reseachByName(dao.searchByFirstname(txtFirstName), txtName), txtRegion), txtPromotion), txtYear));
		}else {
			tableViewPanel.getObservableTrainees().addAll(rs.reseachByYear(rs.reseachByPromotion(rs.reseachByRegion(rs.reseachByName(rs.reseachByFirstName(dao.getAll(),txtFirstName), txtName), txtRegion), txtPromotion), txtYear));
		}
	
	}
	
	
	
	// Show an Information Alert if user choose to edit a trainee
	private void showDialogToEdit(Trainee traineeToEdit) {
		Dialog dialog = new Dialog();
		dialog.setTitle("Mettre à jour un stagiaire");
		dialog.setHeaderText("Mettre à jour les informations du stagiaire");
		
		ButtonType saveButton = new ButtonType("Enregistrer", ButtonData.OK_DONE);
		ButtonType cancelEditButton = new ButtonType("Annuler", ButtonData.CANCEL_CLOSE);
		
		dialog.getDialogPane().getButtonTypes().addAll(saveButton, cancelEditButton);
		
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));
		
		TextField firstName = new TextField(traineeToEdit.getFirstName());
		TextField name = new TextField(traineeToEdit.getName());
		TextField region = new TextField(traineeToEdit.getRegion());
		TextField promo = new TextField(traineeToEdit.getPromotion());
		TextField year = new TextField(traineeToEdit.getYear());

		grid.addRow(0, new Label("Nom :"), firstName);
		grid.addRow(1, new Label("Prénom :"), name);
		grid.addRow(2, new Label("Départment :"), region);
		grid.addRow(3, new Label("Promotion :"), promo);
		grid.addRow(4, new Label("Année :"), year);
		
		dialog.getDialogPane().setContent(grid);
		
		Optional<ButtonType> result = dialog.showAndWait();
		if (result.get() == saveButton){
			traineeToEdit.setFirstName(firstName.getText());
			traineeToEdit.setName(name.getText());
			traineeToEdit.setRegion(region.getText());
			traineeToEdit.setPromotion(promo.getText());
			traineeToEdit.setYear(year.getText());
			
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("MESSAGE");
			boolean isUpdated = dao.updateTrainee(traineeToEdit);
			if(isUpdated) {
				alert.setHeaderText("Mise à jour réussie");
				ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
				alert.getButtonTypes().setAll(okButton);
				alert.showAndWait();
	
			}else {
				alert.setHeaderText("Echec de la mise à jour");
				ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
				alert.getButtonTypes().setAll(okButton);
				alert.showAndWait();
			}

		} else {
			 // ... user chose CANCEL or closed the dialog
		}
		
	}
	

	// Show a Warning Alert if user choose to delete a trainee
		private void showAlertToDelete(Trainee traineeToDelete) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("MESSAGE");
			alert.setHeaderText("Etes-vous sûr de supprimer le stagiaire suivant ?");
			alert.setContentText("NOM : \t\t\t\t"+ traineeToDelete.getFirstName() + 
									"\nPrénom : \t\t\t\t" + traineeToDelete.getName() +
									"\nDépartement : \t\t" + traineeToDelete.getRegion() + 
									"\nPromotion : \t\t\t" + traineeToDelete.getPromotion() +
									"\nYear : \t\t\t\t" + traineeToDelete.getYear()+"\n");

			ButtonType yesButton = new ButtonType("Oui, supprimer", ButtonData.OK_DONE);
			ButtonType cancelDeleteButton = new ButtonType("Annuler", ButtonData.CANCEL_CLOSE);
			alert.getButtonTypes().setAll(yesButton, cancelDeleteButton);
			
			Optional<ButtonType> result = alert.showAndWait();
			Dialog dialog = new Dialog();
			dialog.setTitle("MESSAGE");
			if (result.get() == yesButton){
			    boolean isDeleted = dao.deleteTrainee(traineeToDelete);
			    if(isDeleted) {
					dialog.setHeaderText("Suppression réussie");
					ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
					dialog.getDialogPane().getButtonTypes().addAll(okButton);
					dialog.showAndWait();
			    }else {
					dialog.setHeaderText("Echec de la suppression");
					ButtonType okButton = new ButtonType("OK", ButtonData.OK_DONE);
					dialog.getDialogPane().getButtonTypes().addAll(okButton);
					dialog.showAndWait();
			    }	
			} else {
	
			}
		}
	
	
		/*
		 * Copy fileSource to fileDestination
		 */
		public static void copy ( File source,  File target) throws IOException {  
		     FileChannel sourceChannel = null;  
		     FileChannel targetChannel = null;  
		      try {  
		           sourceChannel = new FileInputStream(source).getChannel();  
		           targetChannel =  new FileOutputStream(target).getChannel();  
		           targetChannel.transferFrom(sourceChannel, 0, sourceChannel.size());  
		        }  
		     finally {  
		        targetChannel.close();  
		        sourceChannel.close();  
		        }  
	}  
		

		
	
	
}
