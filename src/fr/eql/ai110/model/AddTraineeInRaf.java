package fr.eql.ai110.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class AddTraineeInRaf {
	
	private String DestinationFilePath = "./src/fr/eql/ai110/data/stagiaires.RAF";
	
	public boolean addTrainee(Trainee traineeToAdd, int nodeKey) {
		RafManager rm = new RafManager();
		UpdateTraineeInRaf ut = new UpdateTraineeInRaf();
		//Vérification de la taille des champs
		if(traineeToAdd.getFirstName().length() > rm.getFirstNameSize() || traineeToAdd.getName().length() > rm.getNameSize() || 
				traineeToAdd.getRegion().length() > rm.getRegionSize() || traineeToAdd.getPromotion().length() > rm.getPromotionSize() || traineeToAdd.getYear().length() > rm.getYearSize() ) {
			System.out.println("Taille d'un des champs du stagiaire trop grande");
			return false;
		}
		if(traineeToAdd.getFirstName().length() == 0) {
			System.out.println("Champ nom vide");
			return false;
		}

		try {
			RandomAccessFile raf = new RandomAccessFile(this.DestinationFilePath, "rw");
			Trainee nodeTrainee = rm.readTrainee(nodeKey, raf);
			
			//Elements à comparer
			String trainee = traineeToAdd.getFirstName().toUpperCase().replace(" ", "");
			String node = nodeTrainee.getFirstName().toUpperCase().replace(" ", "");
	
			//Si le fichier est vide
			if(nodeTrainee.getFirstName().equals("")) {
				rm.writeTrainee(traineeToAdd, raf);
				return true;
				
			//Si traineeToAdd est inférieur ou égal à noeud courant	
			}else if(trainee.compareTo(node) <= 0){  
				if(nodeTrainee.getFirstNameLeftTree().equals("-1")) {
					//Ecriture de traineeToAdd
					rm.writeTrainee(traineeToAdd, raf);
					//MAJ du SAG de nodeTrainee
					nodeTrainee.setFirstNameLeftTree(traineeToAdd.getTraineeKey());
					ut.updateTrainee(nodeTrainee, Integer.valueOf(nodeTrainee.getTraineeKey()), raf);
					return true;
					
				}else {
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameLeftTree());
					addTrainee(traineeToAdd, nodeKey);
				}
				
			//Si traineeToAdd est supérieur à noeud courant	
			}else if(trainee.compareTo(node) > 0){ 
				if(nodeTrainee.getFirstNameRightTree().equals("-1")) {
					//Ecriture de traineeToAdd
					rm.writeTrainee(traineeToAdd, raf);
					//MAJ du SAD de nodeTrainee
					nodeTrainee.setFirstNameRightTree(traineeToAdd.getTraineeKey());
					ut.updateTrainee(nodeTrainee, Integer.valueOf(nodeTrainee.getTraineeKey()), raf);
					return true;
				
				}else {
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameRightTree());
					addTrainee(traineeToAdd, nodeKey);
				}
				
				
			}else {
				System.out.println("non ajouté : "+traineeToAdd);
				return false;
			}
			raf.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	
	public boolean addTrainee(Trainee traineeToAdd) {
		int root = 0;
		return addTrainee(traineeToAdd, root);
	}
	

}
