package fr.eql.ai110.model;

import java.util.Objects;

public class Trainee {

	private String traineeKey;
	private String firstNameLeftTree;
	private String firstNameRightTree;
	
	private String firstName;
	private String name;
	private String region;
	private String promotion;
	private String year;
	

	
	public Trainee(String firstName, String name, String region, String promotion, String year) {
		super();
		this.traineeKey = "";
		this.firstNameLeftTree = "-1";
		this.firstNameRightTree = "-1";
		this.firstName = firstName;
		this.name = name;
		this.region = region;
		this.promotion = promotion;
		this.year = year;
	}

	

	public Trainee(String traineeKey, String firstNameLeftTree, String firstNameRightTree,String firstName, String name, String region, String promotion, String year) {
		super();
		this.traineeKey = traineeKey;
		this.firstNameLeftTree = firstNameLeftTree;
		this.firstNameRightTree = firstNameRightTree;
		this.firstName = firstName;
		this.name = name;
		this.region = region;
		this.promotion = promotion;
		this.year = year;
	}



	public String getTraineeKey() {
		return traineeKey;
	}


	public void setTraineeKey(String traineeKey) {
		this.traineeKey = traineeKey;
	}


	public String getFirstNameRightTree() {
		return firstNameRightTree;
	}


	public void setFirstNameRightTree(String firstNameRightTree) {
		this.firstNameRightTree = firstNameRightTree;
	}


	public String getFirstNameLeftTree() {
		return firstNameLeftTree;
	}


	public void setFirstNameLeftTree(String firstNameLeftTree) {
		this.firstNameLeftTree = firstNameLeftTree;
	}


	@Override
	public String toString() {
		return "key="+ traineeKey + ", firstNameLeftTree=" + firstNameLeftTree + ", firstNameRightTree=" + firstNameRightTree + ", firstName=" + firstName + ", name=" + name + ", region=" + region + ", promotion=" + promotion
				+ ", year=" + year;
	}




	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getRegion() {
		return region;
	}


	public void setRegion(String region) {
		this.region = region;
	}


	public String getPromotion() {
		return promotion;
	}


	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}


	public String getYear() {
		return year;
	}


	public void setYear(String year) {
		this.year = year;
	}



	@Override
	public int hashCode() {
		return Objects.hash(firstName, firstNameLeftTree, firstNameRightTree, name, promotion, region, traineeKey,
				year);
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trainee other = (Trainee) obj;
		return Objects.equals(firstName, other.firstName) || Objects.equals(name, other.name)
				|| Objects.equals(promotion, other.promotion) || Objects.equals(region, other.region)
				|| Objects.equals(traineeKey, other.traineeKey) || Objects.equals(year, other.year);
	}
	
	
}