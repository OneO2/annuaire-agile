package fr.eql.ai110.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class DisplayRaf {
	
	private String DestinationFilePath = "./src/fr/eql/ai110/data/stagiaires.RAF";
	
	private List<Trainee> list = new ArrayList<Trainee>();
	

	public void displayAlphabeticalOrder(int nodeKey) {
		try {
			
			RandomAccessFile raf = new RandomAccessFile(this.DestinationFilePath, "rw");
			RafManager rm = new RafManager();
			Trainee currentTrainee = rm.readTrainee(nodeKey, raf);
			
			if (!currentTrainee.getFirstNameLeftTree().equals("-1")) {
				displayAlphabeticalOrder(Integer.parseInt(currentTrainee.getFirstNameLeftTree()));
			}
			list.add(currentTrainee);
		
			if (!currentTrainee.getFirstNameRightTree().equals("-1")) {
				displayAlphabeticalOrder(Integer.parseInt(currentTrainee.getFirstNameRightTree()));
			}
			
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<Trainee> getList() {
		int root = 0;
		displayAlphabeticalOrder(root);
		return list;
	}

}
