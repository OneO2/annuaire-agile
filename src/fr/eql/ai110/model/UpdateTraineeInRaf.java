package fr.eql.ai110.model;

import java.io.IOException;
import java.io.RandomAccessFile;

public class UpdateTraineeInRaf {
	
	
	
	public boolean updateTrainee(Trainee trainee, int key, RandomAccessFile raf) {
		boolean isUpdated = false;
		RafManager rm = new RafManager();
		int traineeSize = rm.getFullTraineeInfoSize();
		
		try {
			// je me positionne à la clé
			raf.seek(key*traineeSize);

			// j'écris le stagiaire à partir du curseur positionné
			rm.writeTraineeField(String.valueOf(trainee.getTraineeKey()), rm.getSizeOfKey(), raf);
			rm.writeTraineeField(trainee.getFirstNameLeftTree().toString(), rm.getSizeOfLeftSon(), raf);
			rm.writeTraineeField(trainee.getFirstNameRightTree().toString(), rm.getSizeOfRightSon(), raf);
			rm.writeTraineeField(trainee.getFirstName(), rm.getFirstNameSize(), raf);
			rm.writeTraineeField(trainee.getName(), rm.getNameSize(), raf);
			rm.writeTraineeField(trainee.getRegion(), rm.getRegionSize(), raf);
			rm.writeTraineeField(trainee.getPromotion(), rm.getPromotionSize(), raf);
			rm.writeTraineeField(trainee.getYear(), rm.getYearSize(), raf);
			isUpdated = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isUpdated;
	}

}
