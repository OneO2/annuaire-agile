package fr.eql.ai110.model;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

import sun.applet.Main;



public class TraineeDAO {
	


	private String DestinationFilePath = "./src/fr/eql/ai110/data/stagiaires.RAF";
	private List<Trainee> TraineeInDao = new ArrayList<Trainee>();
	RafManager rm = new RafManager();


	//	DataBaseToDisplay
	public List<Trainee> getAll() {

		DisplayRaf dr = new DisplayRaf();		
		TraineeInDao = dr.getList();

		return TraineeInDao;
	}

	
	// Add trainee
	public boolean addTrainee(Trainee traineeToAdd) {
		AddTraineeInRaf at = new AddTraineeInRaf();
		return at.addTrainee(traineeToAdd);
	}
	
	
	// Update trainee
	public boolean updateTrainee(Trainee traineeToUpdate) {
		boolean isUpdated = false;
		UpdateTraineeInRaf up = new UpdateTraineeInRaf();
		try {
			RandomAccessFile raf = new RandomAccessFile(this.DestinationFilePath, "rw");
			isUpdated = up.updateTrainee(traineeToUpdate, Integer.valueOf(traineeToUpdate.getTraineeKey()), raf);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isUpdated;
	}
	
	
	//Delete trainee
	public boolean deleteTrainee(Trainee traineeToDelete) {
		DeleteTraineeInRaf dt = new DeleteTraineeInRaf();
		return dt.deleteTrainee(traineeToDelete);
	}
	
	
	//Search trainee
	public List<Trainee> searchByFirstname(String firstnameToSearch){
		SearchTrainee st = new SearchTrainee();
		return st.getResultFromSeachByFirstName(firstnameToSearch);
	}
	
	
	
	/*
	 * Getters & Setters
	 */
	
	public List<Trainee> getTraineeInDao() {
		return TraineeInDao;
	}

	public void setTraineeInDao(List<Trainee> traineeInDao) {
		TraineeInDao = traineeInDao;
	}

	
}