package fr.eql.ai110.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class SearchTrainee {
	
private String DestinationFilePath = "./src/fr/eql/ai110/data/stagiaires.RAF";
private TraineeDAO dao = new TraineeDAO();
private List<Trainee> traineesList = dao.getAll();
private List<Trainee> result = new ArrayList<Trainee>();
private int root = 0;


	// Garder la fonction en private 
	// pour faire la recherche, utiliser la fonction getResultFromSeachByFirstName(String firstNameToSearch) (voir tout en bas du fichier)

	private List<Trainee> seachByFirstNameByTree(String firstNameToSearch, int nodeKey) {
		RafManager rm = new RafManager();
		
		try {
			RandomAccessFile raf = new RandomAccessFile(this.DestinationFilePath, "rw");
			Trainee nodeTrainee = rm.readTrainee(nodeKey, raf);
			
			//Mise au format des firstName à comparer : tout en majuscules + suppression des espaces
			firstNameToSearch = firstNameToSearch.toUpperCase().replace(" ", "");
			String firstNameNode = nodeTrainee.getFirstName().toUpperCase().replace(" ", "");
			
			System.out.println(firstNameToSearch + " comparé à " + firstNameNode);

			//Si le fichier est vide
			if(nodeTrainee.getFirstName().equals("")) {
				System.out.println("Recherche impossible : le fichier RAF est vide");
			
			//Si firstNameToSearch est inférieur ou égal à noeud courant	
			}else if(firstNameToSearch.compareTo(firstNameNode) < 0){  
				
				if(nodeTrainee.getFirstNameLeftTree().equals("-1")) {
					return result;
				}else {
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameLeftTree());
					seachByFirstNameByTree(firstNameToSearch, nodeKey);
				}
				
			//Si firstNameToSearch est supérieur à noeud courant	
			}else if(firstNameToSearch.compareTo(firstNameNode) > 0){ 
				
				if(nodeTrainee.getFirstNameRightTree().equals("-1")) {
					return result;
				}else {
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameRightTree());
					seachByFirstNameByTree(firstNameToSearch, nodeKey);
				}
				
				
			}else if(firstNameToSearch.compareTo(firstNameNode) == 0){
				System.out.println("Ajout à la liste de résultats de : "+nodeTrainee);
				result.add(nodeTrainee);
				if(!nodeTrainee.getFirstNameLeftTree().equals("-1")) {
					nodeKey = Integer.valueOf(nodeTrainee.getFirstNameLeftTree());
					seachByFirstNameByTree(firstNameToSearch, nodeKey);
				}
			}else {
				System.out.println("Erreur dans les éléments comparés");
			}
			raf.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
	
	public List<Trainee> getResultFromSeachByFirstName(String firstNameToSearch){
		seachByFirstNameByTree(firstNameToSearch, root);
//		if(result.size()==0) {
//			result = dao.getAll();
//		}
		return result;
	}
	
	

	


}
