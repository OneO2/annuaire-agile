package fr.eql.ai110.model;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Admin {

	private String content;
	private String pathAdminFiles = "./src/fr/eql/ai110/data/";


//	public Admin() {
//		account();
//	}

	public String account() {
		try {
		FileReader fr = new FileReader("./src/fr/eql/ai110/data/Admin.txt");
		int character = 0;
		content = "";
		while ((character = fr.read()) != -1) {
			content += (char) character;
//			
		}
//		System.out.println(content);
		fr.close();
		} catch (IOException e) {
		e.printStackTrace();
		}
		return content;
	}

	public boolean checkAccount(String userName) {
		boolean fileExist = false;
		
		File file = new File(pathAdminFiles + userName + ".txt"); 
	    if(file.exists()){
	    	fileExist = true;
	    }else{
	    	fileExist = false;
	    }

		return fileExist;
	}
	
	
//	public static void main(String[] args) {
//		Admin admin = new Admin();
//		admin.account();
//	}
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}